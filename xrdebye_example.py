import numpy as np
import matplotlib.pyplot as plt
from ase import Atoms,io
from ase.visualize import view
from ase.utils.xrdebye import XrDebye
from ase.utils.xrdebye import wavelengths
from urllib.request import urlopen

#search for cif file: http://www.crystallography.net/cod/result.php
#link = "http://www.crystallography.net/cod/4505482.cif"
folder = 'C:/Users/David/Documents/Python related/cif files for crystal visualization/'
file = '4505482.cif'

LiCoO2 = io.read(folder+file,store_tags=True)

for atom in LiCoO2:
    if(atom.symbol == 'Li'):
        atom.charge = 1
    elif(atom.symbol == 'Co'):
        atom.charge = 3
    elif(atom.symbol == 'O'):
        atom.charge = -2
        
view(LiCoO2,repeat=(5,5,1))

fig,ax = plt.subplots()
big_repeat = (10,10,5)#(36,36,7)#30,30,5 takes 589s for 54000 atoms
LiCoO2_big = LiCoO2*big_repeat

LiCoO2_xrd = XrDebye(atoms=LiCoO2_big, wavelength=wavelengths['CuKa1'])
LiCoO2_xrd.calc_pattern(x=np.arange(10,75,0.1), mode='XRD', verbose=True, brute_force=False, plot_histograms=False)
LiCoO2_xrd.calc_hkl(LiCoO2)
#print(LiCoO2_xrd.hkl)
LiCoO2_xrd.plot_pattern(ax=ax)
LiCoO2_xrd.plot_indices(ax=ax)
#LiCoO2_xrd.write_pattern('C:/Users/David/Desktop/LiCoO2 repeat 36-36-7.txt')

LiCoO2_xrd.plot_plane(miller_indices=np.array([0,0,3]), point=np.array([0,0,1/3]), size_scale=0.1)

plt.show()